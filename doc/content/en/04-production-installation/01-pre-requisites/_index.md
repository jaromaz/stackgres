---
title: Pre-requisites
weight: 1
url: install/prerequisites
description: Details about the requirements to set up the operator.
---

As explained in the [Demo section]({{% relref "02-demo-quickstart/01-setting-up-the-environment/_index.md" %}}), for setting up the Operator and StackGres Cluster, you need to have an
environment on top of which it needs to request the necessary resources.

Starting from version 1.2.x StackGres is able to run on any Kubernetes installation from 1.18 to 1.25 version.

{{% children style="li" depth="1"  description="true" %}}
