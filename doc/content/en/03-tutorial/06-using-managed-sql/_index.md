---
title: Using managedSql
url: tutorial/using-managed-sql
description: Details about how use managedSql section.
weight: 6
chapter: true
---

The managedSql section from [SGCluster]({{% relref "06-crd-reference/01-sgcluster" %}}) allows you to create / modify / remove database objects live from a valid `SQL` scripts.

To achieve this you have the next options:

{{% children style="li" depth="2" description="true" %}}